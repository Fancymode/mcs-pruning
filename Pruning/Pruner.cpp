/*
 * Pruner.cpp
 *
 *  Created on: Nov 11, 2010
 *      Author: rgreen
 */

#include "Pruner.h"
#include "Utils.h"
#include <iostream>

using namespace std;
Pruner::Pruner(int _Np, int _Nt, std::vector<Generator> g, std::vector<Line> l, Classifier* opf, double pload, bool ul) {
    Init(_Np, _Nt, g, l, opf, pload, ul);
}
Pruner::~Pruner() {

    R.clear(); Rbest.clear();
    M.clear(); Mbest.clear();
    G.clear();
}

void Pruner::Init(int _Np, int _Nt, std::vector<Generator> g, std::vector<Line> l, Classifier* opf, double pload, bool ul){
    Np 				 = _Np;
    Nt 				 = _Nt;
    gens 			 = g;
    lines 			 = l;
    classifier 		 = opf;
    pLoad			 = pload;
    oTime 			 = 0;
    searchTime 	 = 0;
    useLines	 = ul;

    lineAdjustment 	 = 1;
    totalVehicles 	 = 1539000;
    penetrationLevel = 0.05;
    rho 			 = 0.8;
    phevPlacement	 = pruning::PP_EVEN_ALL_BUSES;

    stoppingMethod 	 = pruning::SM_GENERATION;
    pruningObj		 = pruning::PO_PROBABILITY;

    prunedProb		 = 0;
    minSlope	 	 = 0;
    maxProb	 		 = 1;
    minProbSlope	 = 0;
    numStatesPruned	 = 0;
    avgStatesPruned	 = 0;

    #ifdef _MSC_VER
        maxStatesPruned	 = std::numeric_limits<double>::infinity();
    #else
        maxStatesPruned	 = INFINITY;
    #endif
    
    useLogging		 = false;
    negateFitness	 = false;
    collisions		 = 0;
    useLocalSearch   = false;

    if(useLines) { Nd = gens.size() + lines.size();}
    else		 { Nd = gens.size();}

    permuteSolutions = false;

    R.resize(Np, std::vector<double>(Nd, 0.0));
    Rbest.resize(Np, std::vector<double>(Nd, 0.0));
    M.resize(Np, 0.0);
    #ifdef _MSC_VER
        Mbest.resize(Np, -std::numeric_limits<double>::infinity());
    #else
        Mbest.resize(Np, -INFINITY);
    #endif

}

void Pruner::Reset(int np, int nt){
    Np 				= np;
    Nt 				= nt;
    numStatesPruned	= 0;
    avgStatesPruned	= 0;
    avgProbPruned   = 0;
    prunedProb		= 0;
    totalIterations = 0;
    collisions		= 0;

    clearTimes();
    clearVectors();
}
void Pruner::Prune(MTRand& mt){
   
}
void Pruner::clearVectors(){
    successStates.clear();
    failureStates.clear();
    sampledStates.clear();

    vAvgStatesPruned.clear();
    vAvgProbPruned.clear();
    vAvgFailedStates.clear();
    statesPruned.clear();
    probPruned.clear();
    fStatesPruned.clear();
}
void Pruner::clearTimes(){
    pruningTime = 0;
    oTime		= 0;
    searchTime  = 0;
}
double Pruner::EvaluateSolution(std::vector<int> curSolution, bool localSearch){
    std::string s = "";
    for(unsigned int i=0; i< curSolution.size(); i++){
        if(curSolution[i] == 0){ s += "0";}
        else{s += "1";}
    }
    return EvaluateSolution(s, localSearch);
}

double Pruner::EvaluateSolution(std::string curSolution, bool localSearch){
    CStopWatch timer;
   
    int		totalUp 		= 0,	// Generators up in a single group
            totalDown 		= 0,	// Total Generators in a single group
            tGens           = 1,
            tUp 			= 1,
            curBus			= 0,
            nextBus			= 0,
            numBuses        = 0,
            threadId;
    double  stateProb 		= 1.0,  // Probability for Calculations
            curtailment     = 0.0,
            excess,
            totalCapacity	= 0.00,
            copy			= 1.00,
            retValue,
            iRetValue;
    bool 	failed 			= false;
    MTRand mt;
    std::vector<double> phevLoad, phevGen;
    std::vector<int>    vCurSolution, vCurSolution_Orig;
    std::vector<int>    genSplits;
    std::vector<double> vSolution;
    std::string c;

    threadId = omp_get_thread_num();

    genSplits.push_back(0);
    if(useLines){ 
        vSolution.resize(gens.size() + lines.size() + 1, 0);
        vCurSolution.resize(gens.size() + lines.size(), 0);
    }else{ 
        vSolution.resize(gens.size() + 1, 0);
        vCurSolution.resize(gens.size(), 0);
    }

    classifier->reset();
    for(unsigned int i=0; i< gens.size(); i++){
        nextBus = gens[i].getBus();
        if(nextBus != curBus){
            numBuses++;
            curBus = nextBus;
            copy  *= Match::combination(tGens, tUp);
            tUp    = 0; 
            tGens  = 0;
        }
        tGens++;

        if(i>0 && gens[i] != gens[i-1]){genSplits.push_back(i);}

        if(curSolution[i] == '1'){
            stateProb 	    *= (1-gens[i].getOutageRate());
            totalCapacity   += gens[i].getPG()/100;
            vSolution[i]    = 1;
            vCurSolution[i] = 1;
            tUp++; totalUp++;
        }else{
            stateProb 	*=  gens[i].getOutageRate();
            vSolution[i] = 0;
            vCurSolution[i] = 0;
            totalDown++;
        }
    }
    numBuses++;

    if(useLines && lines.size() > 0){
        for(unsigned int i=0; i<lines.size(); i++){
            if(curSolution[gens.size() + i] == '1'){
                stateProb *= (1-(lines[i].getOutageRate()*lineAdjustment));
                vSolution[gens.size() + i] = 1;
            }else{
                stateProb *=  (lines[i].getOutageRate()*lineAdjustment);
                vSolution[gens.size() + i] = 1;
            }
        }
    }

    timer.startTimer();
    if(successStates.find(curSolution) != successStates.end() || failureStates.find(curSolution) != failureStates.end()){
        collisions++;
        if(pruningObj == pruning::PO_CURTAILMENT && !negateFitness) { return 1000;}
        else                                                        { return -1000;}
    }
    
    timer.stopTimer();
    searchTime += timer.getElapsedTime();

    timer.startTimer();

    if(usePHEVs){
        PHEV::calculatePHEVLoad(penetrationLevel,rho, totalVehicles, numBuses, phevLoad, phevGen, mt, phevPlacement);
        classifier->addLoad(phevLoad);
    }
    if(useLocalSearch && sampledStates.find(curSolution) != sampledStates.end()){
        if(sampledStates[curSolution] == 0) {failed = true;}
        else                                {failed = false;}
    }else{
        curtailment = classifier->run(curSolution,excess);
        if(curtailment != 0.0){ failed = true;}
        else                  { failed = false;
        }
    }
    timer.stopTimer();
    oTime += timer.getElapsedTime();

    #pragma omp critical
    {
        if(failed){
            failureStates.insert(std::pair<std::string, double>(curSolution, stateProb));
            sampledStates.insert(std::pair<std::string, int>(curSolution, 0));
        }else{
            successStates.insert(std::pair<std::string, double>(curSolution, stateProb));
            sampledStates.insert(std::pair<std::string, int>(curSolution, 1));
        }
    }
    copy *= stateProb;

    if(pruningObj == pruning::PO_PROBABILITY)	  { retValue = stateProb;}
    else if(pruningObj == pruning::PO_CURTAILMENT){ retValue = -curtailment;}
    else if(pruningObj == pruning::PO_COPY)		  { retValue = copy;}
    else if(pruningObj == pruning::PO_EXCESS)	  { retValue = excess;}
    else if(pruningObj == pruning::PO_ZERO)		  { retValue = 0;}
    else if(pruningObj == pruning::PO_TUP)        { retValue = totalUp;}
    else										  { retValue = stateProb;}

    if(negateFitness){ retValue *= -1;}

    // Permutations
    if(permuteSolutions){
        if(failed){
            for(unsigned int i=0; i<genSplits.size()-1; i++){  
                while(next_permutation(curSolution.begin()+genSplits[i],    curSolution.begin()+genSplits[i+1])) { 
                    failureStates.insert(std::pair<std::string, double>(curSolution, stateProb));
                }
            }
        }else{
            for(unsigned int i=0; i<genSplits.size()-1; i++){  
                while(next_permutation(curSolution.begin()+genSplits[i],    curSolution.begin()+genSplits[i+1])) { 
                    successStates.insert(std::pair<std::string, double>(curSolution, stateProb));
                }
            }
        }
    }

   // Local Search
   if(useLocalSearch && !failed && localSearch){      
        iRetValue = 0;
        vCurSolution_Orig = vCurSolution;
        while(iRetValue <= 0 && next_permutation(vCurSolution.begin(), vCurSolution.end())){
            iRetValue = EvaluateSolution(vCurSolution, false);
        }

        iRetValue = 0;
        vCurSolution = vCurSolution_Orig;
        while(iRetValue <= 0 && prev_permutation(vCurSolution.begin(), vCurSolution.end())){
            iRetValue = EvaluateSolution(vCurSolution_Orig, false);
        }
        
    }
    return retValue;
}

bool Pruner::isConverged(){
    bool retValue = false;

    if(totalIterations > 1){
        avgStatesPruned = (double)successStates.size()/(double)totalIterations;
        avgStatesFailed = (double)failureStates.size()/(double)totalIterations;
        avgProbPruned 	= getPrunedProb()/totalIterations;
    }
    if(useLogging && totalIterations > 1){
        fStatesPruned   .push_back(failureStates.size());
        vAvgFailedStates.push_back(avgStatesFailed);
        statesPruned	.push_back(successStates.size());
        vAvgStatesPruned.push_back(avgStatesPruned);
        probPruned		.push_back(getPrunedProb());
        vAvgProbPruned	.push_back(avgProbPruned);
        fCollisions.push_back(collisions);
    }else if(useLogging && totalIterations <= 1){
        vAvgStatesPruned.clear();  vAvgProbPruned.clear();     	vAvgFailedStates.clear();
        statesPruned.clear();      probPruned.clear();	       	fStatesPruned.clear();
        stdDevFitness.clear();     hammingDiversity.clear();	fCollisions.clear();
        collisions = 0;
    }

    if(stoppingMethod == pruning::SM_PROB_MAX)			 { if(getPrunedProb() > maxProb)			 {retValue = true;}
    }else if(stoppingMethod == pruning::SM_PROB_SLOPE)   { if(avgProbPruned < minProbSlope) 		 {retValue = true;}
    }else if(stoppingMethod == pruning::SM_STATES_SLOPE) { if(avgStatesPruned < minSlope)			 {retValue = true;}
    }else if(stoppingMethod == pruning::SM_STATES_PRUNED){ if(successStates.size() > maxStatesPruned){retValue = true;}
    }else if(stoppingMethod == pruning::SM_GENERATION)	 { if(totalIterations >= Nt)				 {retValue = true;}
    }

    return retValue;
}

void Pruner::writeLog(std::string fileName){
    if(!useLogging) return;

    ofstream logFile;

    logFile.open(fileName.c_str(), ios::trunc);
    if(logFile.is_open()){
        logFile << "\"Iteration\" \"Collisions\" ";
        logFile << "\"States Pruned\" \"Avg. States Pruned\" \"Slope States Pruned\" ";
        logFile << "\"Failed States\" \"Avg. Failed States\" \"Slope Failed States\" ";
        logFile << "\"Prob. Pruned\" \"Avg. Prob. Pruned\" \"Slope Prob.\" ";
        logFile << "\"Std. Dev. Fitness\" \"Avg. Hamming Distance\" ";
        logFile << "\n";
        for(unsigned int i=0; i<statesPruned.size();i++){

            logFile << setprecision(7) << fixed;
            logFile << i+1 					<< " ";

            if(fCollisions.size() > i)   { logFile << fCollisions[i] << " ";}
            else                         { logFile << "N/A ";}

            logFile	<< statesPruned[i] 		<< " "
                    << vAvgStatesPruned[i] 	<< " ";
            if(i > 0){ logFile << statesPruned[i] - statesPruned[i-1] << " ";
            }else    { logFile << statesPruned[i] << " ";}

            logFile	<< fStatesPruned[i] 		<< " "
                    << vAvgFailedStates[i] 	<< " ";
            if(i > 0){ logFile << fStatesPruned[i] - fStatesPruned[i-1] << " ";
            }else    { logFile << fStatesPruned[i] << " ";}

            logFile << probPruned[i] 		<< " "
                    << vAvgProbPruned[i] 	<< " ";
            if(i > 0){ logFile << probPruned[i] - probPruned[i-1] << " ";
            }else    { logFile << probPruned[i] << " ";}

            if(stdDevFitness.size() > i)   { logFile << stdDevFitness[i] << " ";}
            else                           { logFile << "N/A ";}
            if(hammingDiversity.size() > i){ logFile << hammingDiversity[i] << " ";}
            else                           { logFile << "N/A ";}

            logFile << "\n";
        }
    }
    logFile.close();
}

double Pruner::getStatesSlope(){ return avgStatesPruned;}
double Pruner::getProbSlope()  { return avgProbPruned;}
double Pruner::getPrunedProb() {
    map<std::string, double>::iterator p;

    prunedProb = 0;
    for(p = successStates.begin(); p != successStates.end(); p++) {
        prunedProb += p->second;
    }
    return prunedProb;
}
double Pruner::getFailedProb(){
    map<std::string, double>::iterator p;

    double failedProb = 0;
    for(p = failureStates.begin(); p != failureStates.end(); p++) {
        failedProb += p->second;
    }
    return failedProb;
}

double Pruner::getPruningTime()								{ return pruningTime;}
std::map<std::string, double> Pruner::getSuccessStates()	{ return successStates;}
std::map<std::string, double> Pruner::getFailureStates()	{ return failureStates;}
double Pruner::getTotalIterations()							{ return totalIterations;}  

void Pruner::setUsePHEVs(bool ul)						 	{ usePHEVs = ul;}
void Pruner::setTotalVehicles(int tv)					 	{ totalVehicles = tv;}
void Pruner::setPenetrationLevel(double p)				 	{ penetrationLevel = p;}
void Pruner::setNumBuses(int nb)						 	{ numBuses = nb;}
void Pruner::setRho(double p)							 	{ rho = p;}
void Pruner::setPHEVPlacement(pruning::PHEV_PLACEMENT p) 	{ phevPlacement = p;}

void Pruner::setLineAdjustment(double la)  				 	{ lineAdjustment = la;}
void Pruner::setuseLocalSearch(bool ul)						{ useLocalSearch = ul; }
void Pruner::setObjective(pruning::PRUNING_OBJECTIVE p)	 	{ pruningObj = p;}

void Pruner::setStoppingMethod(pruning::STOPPING_METHOD s)	{ stoppingMethod = s;}
void Pruner::setMinStatesSlope(double s)					{ minSlope = s;}
void Pruner::setMaxStatesPruned(double s)					{ maxStatesPruned = s;}
void Pruner::setMaxProbPruned(double p)						{ maxProb = p;}
void Pruner::setMinProbSlope(double p)						{ minProbSlope = p;}
void Pruner::setMaxGenerations(int i)						{ Nt = i;}
void Pruner::setNumThreads(int i){ 
    numThreads = i;
}

void Pruner::setUseLogging(bool b)							{ useLogging = b;}
void Pruner::setNegateFitness(bool b)						{ negateFitness = b;}
void Pruner::setUseLocalSearch(bool b)						{ useLocalSearch = b;}
void Pruner::setPermuteSolutions(bool b)                    { permuteSolutions = b;}

void Pruner::setStoppingValue(double sv){

    if(stoppingMethod == pruning::SM_PROB_MAX)          { setMaxProbPruned(sv);}
    else if(stoppingMethod == pruning::SM_PROB_SLOPE)   { setMinProbSlope(sv);}
    else if(stoppingMethod == pruning::SM_STATES_SLOPE) { setMinStatesSlope(sv);}
    else if(stoppingMethod == pruning::SM_STATES_PRUNED){ setMaxStatesPruned(sv);}
    else if(stoppingMethod == pruning::SM_GENERATION)   { setMaxGenerations((int)sv);}
}
