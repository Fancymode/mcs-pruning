/*
 * MO_PSO_Sampler.cpp
 *
 *  Created on: Mar 4, 2011
 *      Author: rgreen
 */

#include "MO_PSO_Sampler.h"

#include <string>
#include <iostream>
MO_PSO_Sampler::MO_PSO_Sampler(int _Np, int _Nd, int _Nt) :PSO_Sampler(_Np, _Nd, _Nt) {
	// TODO Auto-generated constructor stub

	C1 = 0.5;
	C2 = 0.5;
	C3 = 0.05;
	C4 = 0.05;
//	C1 = 3.05;
//	C2 = -3.05;
//	C3 = 3.05;
//	C4 = 3.05;

	epsilon = pow(10.0,-7.0);// 0.0000001;

}

MO_PSO_Sampler::~MO_PSO_Sampler() {
	// TODO Auto-generated destructor stub
}

void MO_PSO_Sampler::initPopulation(MTRand& mt) {

	swarm.clear(); gBest.clear(); gBestValues.clear();

	gBest.resize(2, std::vector<int>(gens.size(), 0));
	#ifdef _MSC_VER
		gBestValues.push_back(-std::numeric_limits<double>::infinity()); // Max
		gBestValues.push_back(-std::numeric_limits<double>::infinity()); // Max
	#else
		gBestValues.push_back(-INFINITY); // Max
		gBestValues.push_back(-INFINITY); // Max
	#endif


	for(int i=0; i<Np; i++){
		MO_BinaryParticle p;

		std::vector<int> t1(Nd, 0), t2(Nd, 0), t3(Nd, 0);
		p.pBest.push_back(t1);
		p.pBest.push_back(t2);
		
		#ifdef _MSC_VER
			p.pBestValues.push_back(-std::numeric_limits<double>::infinity()); // Max
			p.pBestValues.push_back(-std::numeric_limits<double>::infinity()); // Max
		#else
			p.pBestValues.push_back(-INFINITY); // Max
			p.pBestValues.push_back(-INFINITY); // Max
		#endif

		for(int j=0; j<(int)gens.size(); j++){
			p.pos.push_back((mt.rand() < gens[j].getOutageRate() ? 0 : 1 ));
			p.vel.push_back(gens[j].getOutageRate());
		}

		for(int j=0; j<(int)lines.size(); j++){
			p.pos.push_back(1);
			p.vel.push_back(lines[j].getOutageRate());
		}
		swarm.push_back(p);
	}
}
bool MO_PSO_Sampler::checkConvergence(){
	bool retValue = false;
	double totalProbDown = 0, totalProbUp=0;

	std::map<std::string, int>::iterator i;
	int count = 0;
	for(i=sampledStates.begin(); i!= sampledStates.end(); i++){
		if(i->second == 0) {
			totalProbDown += sampledStateProbs[i->first];
			count ++;
		}else{
			totalProbUp += sampledStateProbs[i->first];
		}
	}
	pLOLP 	= LOLP;
	LOLP 	= totalProbDown;
	NLOLP 	= totalProbUp;
	//LOLP 	= LOLP/(LOLP+NLOLP);

	change = fabs(LOLP - pLOLP);
	std::cout 	<< sampledStates.size() << " "
				<< sampledStates.size()/pow(2.0,32.0) << " "
				<<  LOLP << " "
				<< 1.0-NLOLP << " "
				<< (1-NLOLP+LOLP)/2.0 << " "
				<< LOLP/(LOLP+NLOLP) << " ";

//	if(change < 0.00005){
//		 std::cout << 1 << " ";
//	}else{
//		std::cout << 0;
//	}
	std::cout << std::endl;
	numSamples = sampledStates.size();


	if(sigma < tolerance){
		retValue = true;
	}
	return retValue;
}
void MO_PSO_Sampler::evaluateFitness(){
	bool failed;
	long double sol, stateProb;
	double totalCap, excess;
	std::string curSolution;
	std::vector<double> vCurSolution;

	vCurSolution.resize(gens.size() + lines.size() + 2, 0);
	for(unsigned int i=0; i<swarm.size(); i++){

		stateProb = 1; totalCap = 0;
		curSolution = String::vectorToString(swarm[i].pos);
		genCount = 0; lineCount = 0;

		for(unsigned int j=0; j<gens.size(); j++){
			vCurSolution[j] = swarm[i].pos[j];
			if(vCurSolution[j] == 1){
				totalCap    += gens[j].getPG()/100;
				stateProb   *= (1-gens[j].getOutageRate());
			}else{
				stateProb *= gens[j].getOutageRate();
				genCount++;
			}
		}
		avgGenCount += genCount;
		genOutageCounts[genCount]++;
		if(lines.size() > 0){
			for(unsigned int j=0; j<lines.size(); j++){
				vCurSolution[j+gens.size()] = swarm[i].pos[j+gens.size()];

				if(vCurSolution[j] == 1){
					vCurSolution[j+gens.size()] = 1;
					stateProb *= (1-lines[j].getOutageRate()*lineAdjustment);
					curSolution += "1";
				}else{
					curSolution += "0";
					vCurSolution[j+gens.size()] = 0;
					stateProb *= (lines[j].getOutageRate()*lineAdjustment);
					lineCount++;
				}
			}
			avgLineCount += lineCount;
			tLineOutageCounts[lineCount]++;
		}

		vCurSolution[gens.size()+lines.size()] 	 = stateProb;
		vCurSolution[gens.size()+lines.size()+1] = totalCap;

		timer1.startTimer();

		if(successStates.find(curSolution) != successStates.end()){
			Collisions++;
			continue;
		}
		timer1.stopTimer();
		searchTime += timer1.getElapsedTime();

	
		classifier->init();
		sol = classifier->run(vCurSolution, excess);
		if(sol != 0.0){ failed = true;}
        else          { failed = false;}
		sol = excess;
	
		if(stateProb < epsilon){
			stateProb = epsilon;
			failed = true;
			sol = pLoad/2.0;
		}
		sampledStateProbs[curSolution] = stateProb;
		if(stateProb*100 > swarm[i].pBestValues[0]){
			swarm[i].pBest[0]      	= swarm[i].pos;
			swarm[i].pBestValues[0] = stateProb*100;
		}
		if(sol > swarm[i].pBestValues[1]){
			swarm[i].pBest[1]      	= swarm[i].pos;
			swarm[i].pBestValues[1] = sol;
		}

		if(stateProb*100 > gBestValues[0]){
			gBest[0] 		= swarm[i].pos;
			gBestValues[0] 	= stateProb*100;
		}
		if(sol >swarm[i].pBestValues[1]){
			gBest[1] 		= swarm[i].pos;
			gBestValues[1] 	= sol;
		}

		if(failed){
			sampledStates[curSolution] = 0;
			uniqueStates["1" + curSolution] = vCurSolution;
		}else{
			sampledStates[curSolution] = 1;
			uniqueStates["0" + curSolution] = vCurSolution;
		}
	}
}
void MO_PSO_Sampler::updatePositions(MTRand& mt){
	double R1, R2, R3, newV;
	for(unsigned int i=0; i< swarm.size(); i++){
		for(int j=0; j<Nd; j++){

			R1 = mt.rand();	R2 = mt.rand(); R3 = mt.rand();

			 newV = swarm[i].vel[j] +
				C1 * mt.rand() * (swarm[i].pBest[0][j] 	- swarm[i].pos[j]) +
				C2 * mt.rand() * (gBest[0][j] 			- swarm[i].pos[j]) +
				C3 * mt.rand() * (swarm[i].pBest[1][j] 	- swarm[i].pos[j]) +
				C4 * mt.rand() * (gBest[1][j] 			- swarm[i].pos[j]);

			newV = sigMoid(newV);
			swarm[i].vel[j] = newV;

			if(newV > mt.rand()){
				swarm[i].pos[j] = 1;
			}else{
				swarm[i].pos[j] = 0;
			}
		}
	}
}

void MO_PSO_Sampler::run(MTRand& mt){
	bool converged = false;
	int j;

	stateGenerationTime = searchTime = 0;
	tLineOutageCounts.clear(); genOutageCounts.clear();
	tLineOutageCounts.resize(lines.size(), 0); genOutageCounts.resize(gens.size(), 0);

	timer.startTimer();

	sumX=0; sumXSquared=0; nSumX=0; nSumXSquared=0;
	curProb 	 = 0.0; vLOLP 		 = 0.0; pLOLP = 0.0;
	numSamples 	 = 1; 	iterations 	 = 0;	sigma = 1.0;
	LOLP 		 = 0.0; pLOLP = 0.0;
	Collisions	 = 0;	avgLineCount = 0;	avgGenCount  = 0;

	uniqueStates.clear(); sampledStateProbs.clear(); sampledStates.clear();

	j=0;
	initPopulation(mt);

	//while(!converged){
	while(j < Nt){
		if(j%1000==0){
			//std::cin.ignore(1,'\n');
		}
		std::cout << j << " ";
		evaluateFitness();
		updatePositions(mt);
		converged = checkConvergence();
		converged = false;
		j++;
	}
	totalIterations = j;
	iterations = j;
	timer.stopTimer();
	simulationTime = timer.getElapsedTime();
}
